# Rôle wordpress

Rôle permettant l'initialisation et le maintiens d'une instance wordpress
en multi site, en utilisant wp-cli.

Pour installer ce rôle, il faut l'ajouter comme sous-module dan un
répertoire role. Les variables par défauts sont dans defaults, et elles
sont surchargées dans les répertoires group_vars et host_vars.

## Dépendences

- Apache2
- PHP5+
- mariadb
- letsencrypt
